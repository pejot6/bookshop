﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using BookShop.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace BookShop.DAL
{
    public class BookShopContext : IdentityDbContext<ApplicationUser>
    {
        public BookShopContext() : base("BookShopConnectionString")
        {
            
        }

        static BookShopContext()
        {
            Database.SetInitializer<BookShopContext>(new BookShopInitializer());
        }

        public static BookShopContext Create()
        {
            return new BookShopContext();
        }

        public DbSet<Book> Books { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderPosition> OrderPositions { get; set; }
    }
}