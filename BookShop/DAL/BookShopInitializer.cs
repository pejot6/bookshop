﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using BookShop.Migrations;
using BookShop.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace BookShop.DAL
{
    public class BookShopInitializer : MigrateDatabaseToLatestVersion<BookShopContext, Configuration>
    {
        //protected override void Seed(BookShopContext context)
        //{
        //    SeedBookData(context);
        //    base.Seed(context);
        //}

        public static void SeedBookData(BookShopContext context)
        {
            var categories = new List<Category>
            {
                new Category() {CategoryId = 1, Name = "Fantasy", Description = "Magia i Miecz", NamePic = "fantasy.jpg"},
                new Category() {CategoryId = 2, Name = "Sci-Fi", Description = "Technologia i Nauka", NamePic = "scifi.jpg"},
                new Category() {CategoryId = 3, Name = "Programowanie", Description = "Mów językiem komptuerów", NamePic = "programowanie.jpg"},
                new Category() {CategoryId = 4, Name = "Powieść", Description = "Życie jest nowelą", NamePic = "novel.jpg"},
                new Category() {CategoryId = 5, Name = "Bibliografia", Description = "Czy to ciekawy człowiek?", NamePic = "bibliografia.jpg"}
            };
            categories.ForEach(x=>context.Categories.AddOrUpdate(x));
            context.SaveChanges();

            var books = new List<Book>
            {
                new Book() {BookId = 1, Title = "Sługa Boży", Writer = "Jacek Piekara", CategoryId = 1, Price = 50, Bestseller = true, NamePic = "inkwizytor.jpg", Description = "Jezus zszedł z krzyża i pokarał swoich wrogów", AddDate = DateTime.Now},
                new Book() {BookId = 2, Title = "Powrót z Gwiazd", Writer = "Stanisław Lem", CategoryId = 2, Price = 50, Bestseller = true, NamePic = "gwiazd.jpg", Description = "Podróże kosmiczne dobiegają końca...", AddDate = DateTime.Now},
                new Book() {BookId = 3, Title = "ASP.NET MVC 4 Zaawansowane Programowanie", Writer = "Adam Freeman", CategoryId = 3, Price = 10, Bestseller = false, NamePic = "mvc.jpg", Description = "Aplikacje Internetowe", AddDate = DateTime.Now},
                new Book() {BookId = 4, Title = "Martin Eden", Writer = "Jack London", CategoryId = 4, Price = 50, Bestseller = true, NamePic = "martin.jpg", Description = "Upór i ciężka praca - czy popłaca?", AddDate = DateTime.Now},
                new Book() {BookId = 5, Title = "Elon Musk", Writer = "Ashlee Vance", CategoryId = 5, Price = 50, Bestseller = false, NamePic = "musk.jpg", Description = "Od programowania to lotów w kosmos", AddDate = DateTime.Now},
                new Book() {BookId = 6, Title = "Ostatnie Życzenie", Writer = "Andrzej Sapkowski", CategoryId = 1, Price = 50, Bestseller = true, NamePic = "wiedzmin.jpg", Description = "Pierwsza część Wiedźmina!", AddDate = DateTime.Now}
            };
            books.ForEach(x=>context.Books.AddOrUpdate(x));
            context.SaveChanges();
        }

        public static void SeedUzytkownicy(BookShopContext db)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));

            const string name = "admin@bookshop.pl";
            const string password = "P@ssw0rd";
            const string roleName = "Admin";

            var user = userManager.FindByName(name);
            if (user == null)
            {
                user = new ApplicationUser { UserName = name, Email = name, UserData = new UserData() };
                var result = userManager.Create(user, password);
            }

            // utworzenie roli Admin jeśli nie istnieje 
            var role = roleManager.FindByName(roleName);
            if (role == null)
            {
                role = new IdentityRole(roleName);
                var roleresult = roleManager.Create(role);
            }

            // dodanie uzytkownika do roli Admin jesli juz nie jest w roli
            var rolesForUser = userManager.GetRoles(user.Id);
            if (!rolesForUser.Contains(role.Name))
            {
                var result = userManager.AddToRole(user.Id, role.Name);
            }
        }
    }
}