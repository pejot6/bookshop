namespace BookShop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Orders : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Orders", name: "ApplicationUser_Id", newName: "UserId");
            RenameIndex(table: "dbo.Orders", name: "IX_ApplicationUser_Id", newName: "IX_UserId");
            AddColumn("dbo.AspNetUsers", "UserData_Name", c => c.String());
            AddColumn("dbo.AspNetUsers", "UserData_Surname", c => c.String());
            AddColumn("dbo.AspNetUsers", "UserData_Street", c => c.String());
            AddColumn("dbo.AspNetUsers", "UserData_City", c => c.String());
            AddColumn("dbo.AspNetUsers", "UserData_ZipCode", c => c.String());
            AlterColumn("dbo.Orders", "Telephone", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.Orders", "Email", c => c.String(nullable: false));
            DropColumn("dbo.AspNetUsers", "UserData_FirstName");
            DropColumn("dbo.AspNetUsers", "UserData_LastName");
            DropColumn("dbo.AspNetUsers", "UserData_Adress");
            DropColumn("dbo.AspNetUsers", "UserData_Town");
            DropColumn("dbo.AspNetUsers", "UserData_PostCode");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "UserData_PostCode", c => c.String());
            AddColumn("dbo.AspNetUsers", "UserData_Town", c => c.String());
            AddColumn("dbo.AspNetUsers", "UserData_Adress", c => c.String());
            AddColumn("dbo.AspNetUsers", "UserData_LastName", c => c.String());
            AddColumn("dbo.AspNetUsers", "UserData_FirstName", c => c.String());
            AlterColumn("dbo.Orders", "Email", c => c.String());
            AlterColumn("dbo.Orders", "Telephone", c => c.String());
            DropColumn("dbo.AspNetUsers", "UserData_ZipCode");
            DropColumn("dbo.AspNetUsers", "UserData_City");
            DropColumn("dbo.AspNetUsers", "UserData_Street");
            DropColumn("dbo.AspNetUsers", "UserData_Surname");
            DropColumn("dbo.AspNetUsers", "UserData_Name");
            RenameIndex(table: "dbo.Orders", name: "IX_UserId", newName: "IX_ApplicationUser_Id");
            RenameColumn(table: "dbo.Orders", name: "UserId", newName: "ApplicationUser_Id");
        }
    }
}
