﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BookShop.Models;
using Postal;

namespace BookShop.ViewModels
{
    public class ConfirmOrderEmail : Email
    {
        public string To { get; set; }
        public string From { get; set; }
        public decimal Value { get; set; }
        public int OrderNumber { get; set; }
        public List<OrderPosition> OrderPosition { get; set; }
    }
}