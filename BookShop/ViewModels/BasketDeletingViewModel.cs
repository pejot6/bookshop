﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookShop.ViewModels
{
    public class BasketDeletingViewModel
    {
        public decimal BasketTotalPrice { get; set; }
        public int BasketNumberOfItems { get; set; }
        public int NumberOfDeletingItems { get; set; }
        public int IdItemDeleting { get; set; }
    }
}