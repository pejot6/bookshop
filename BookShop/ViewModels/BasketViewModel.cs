﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BookShop.Models;

namespace BookShop.ViewModels
{
    public class BasketViewModel
    {
        public List<BasketItem> BasketItems { get; set; }
        public decimal TotalPrice { get; set; }
    }
}