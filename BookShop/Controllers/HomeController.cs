﻿using System;
using System.Collections.Generic;
using System.EnterpriseServices;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BookShop.DAL;
using BookShop.Infrastructure;
using BookShop.Models;
using BookShop.ViewModels;

namespace BookShop.Controllers
{
    public class HomeController : Controller
    {
        private BookShopContext db = new BookShopContext();

        public ActionResult Index()
        {
            ICacheProvider cache = new DefaultCacheProvider();

            //category
            List<Category> categories;

            if (cache.IsSet(Const.CategoryCacheKey))
            {
                categories = cache.Get(Const.CategoryCacheKey) as List<Category>;
            }
            else
            {
                categories = db.Categories.ToList();
                cache.Set(Const.CategoryCacheKey, categories, 60);
            }

            //new
            List<Book> newBooks;

            if (cache.IsSet(Const.NewCacheKey))
            {
                newBooks = cache.Get(Const.NewCacheKey) as List<Book>;
            }
            else
            {
                newBooks = db.Books.Where(x => !x.Hidden).OrderByDescending(x => x.AddDate).Take(3).ToList();
                cache.Set(Const.NewCacheKey, newBooks, 60);
            }

            //bestsellers
            List<Book> bestsellerBooks;

            if (cache.IsSet(Const.BestsellerCacheKey))
            {
                bestsellerBooks = cache.Get(Const.BestsellerCacheKey) as List<Book>;
            }
            else
            {
                bestsellerBooks = db.Books.Where(x => !x.Hidden && x.Bestseller).OrderBy(x => Guid.NewGuid()).Take(3).ToList();
                cache.Set(Const.BestsellerCacheKey, bestsellerBooks, 60);
            }
            

            var vm = new HomeViewModel()
            {
                Categories = categories,
                Books = newBooks,
                Bestsellers = bestsellerBooks
            };
            return View(vm);
        }

        public ActionResult StaticPages(string name)
        {
            return View(name);

        }
      
    }
}