﻿using System;
using System.Collections.Generic;
using System.EnterpriseServices;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BookShop.DAL;
using BookShop.Infrastructure;
using BookShop.ViewModels;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using BookShop.App_Start;
using BookShop.Models;
using Microsoft.AspNet.Identity.Owin;

namespace BookShop.Controllers
{
    public class BasketController : Controller
    {
        private BasketManager basketManager;
        private ISessionManager sessionManager { get; set; }
        private BookShopContext db;

        public BasketController()
        {
            db = new BookShopContext();
            sessionManager = new SessionManager();
            basketManager = new BasketManager(sessionManager, db);
        }

        // GET: Basket
        public ActionResult Index()
        {
            var basketItems = basketManager.DownloadBasket();
            var totalPrice = basketManager.DownloadBasketValue();
            BasketViewModel basketVM = new BasketViewModel()
            {
                BasketItems = basketItems,
                TotalPrice = totalPrice
            };
            return View(basketVM);
        }

        public ActionResult AddToBasket(int id)
        {
            basketManager.AddToBasket(id);

            return RedirectToAction("Index");
        }

        public int DownloadNumberOfBasketItems()
        {
            return basketManager.DownloadNumberBasketItem();
        }

        public ActionResult DeleteFromBasket(int bookIdd)
        {
            int numberOfItems = basketManager.DeleteFromBasket(bookIdd);
            int numberOfBasketItems = basketManager.DownloadNumberBasketItem();
            decimal basketValue = basketManager.DownloadBasketValue();

            var score = new BasketDeletingViewModel
            {
                IdItemDeleting = bookIdd,
                NumberOfDeletingItems = numberOfItems,
                BasketTotalPrice = basketValue,
                BasketNumberOfItems = numberOfBasketItems,
            };
            return Json(score);
        }

        public async Task<ActionResult> Pay()
        {
            if (Request.IsAuthenticated)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

                var order = new Order
                {
                    Name = user.UserData.Name,
                    Surname = user.UserData.Surname,
                    Street = user.UserData.Street,
                    City = user.UserData.City,
                    ZipCode = user.UserData.ZipCode,
                    Telephone = user.UserData.Telephone,
                    Email = user.UserData.Email
                };
                return View(order);
            }
            else
            {
                return RedirectToAction("Login", "Account", new {returnUrl = Url.Action("Pay", "Basket")});
            }
        }

        [HttpPost]
        public async Task<ActionResult> Pay(Order orderDetails)
        {
            if (ModelState.IsValid)
            {
                // pobieramy id uzytkownika aktualnie zalogowanego
                var userId = User.Identity.GetUserId();

                // utworzenie obiektu zamowienia na podstawie tego co mamy w koszyku
                var newOrder = basketManager.AddOrder(orderDetails, userId);

                // szczegóły użytkownika - aktualizacja danych 
                var user = await UserManager.FindByIdAsync(userId);
                TryUpdateModel(user.UserData);
                await UserManager.UpdateAsync(user);

                // opróżnimy nasz koszyk zakupów
                basketManager.EmptyBasket();

                var order =
                    db.Orders.Include("OrderPositions")
                        .Include("OrderPositions.Book")
                        .SingleOrDefault(x => x.OrderId == newOrder.OrderId);

                ConfirmOrderEmail email = new ConfirmOrderEmail();
                email.To = order.Email;
                email.From = "bookshop@o2.pl";
                email.Value = order.TotalPrice;
                email.OrderNumber = order.OrderId;
                email.OrderPosition = order.OrderPositions;
                email.Send();

                //maileService.WyslaniePotwierdzenieZamowieniaEmail(newOrder);

                return RedirectToAction("ConfirmOrder");
            }
            else
                return View(orderDetails);
        }

        public ActionResult ConfirmOrder()
        {
            return View();
        }

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
    }
}