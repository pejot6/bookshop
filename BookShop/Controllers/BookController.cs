﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BookShop.DAL;

namespace BookShop.Controllers
{
    public class BookController : Controller
    {
        private BookShopContext db = new BookShopContext();
        // GET: Books
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List(string name, string searchQuery = null)
        {
            var category = db.Categories.Include("Books").Where(k => k.Name.ToUpper() == name.ToUpper()).Single();

            //var books = category.Books.ToList();

            var books = category.Books.Where(x => (searchQuery == null ||
                                                   x.Title.ToLower().Contains(searchQuery.ToLower()) ||
                                                   x.Writer.ToLower().Contains(searchQuery.ToLower())) &&
                                                  !x.Hidden);

            if (Request.IsAjaxRequest())
            {
                return PartialView("_BooksList", books);
            }

            return View(books);
        }

        public ActionResult Details(int id)
        {
            var book = db.Books.Find(id);
            return View(book);
        }

        [ChildActionOnly]
        [OutputCache(Duration = 86400)]
        public ActionResult CategoriesMenu()
        {
            
            var categories = db.Categories.ToList();
            return PartialView("_CategoriesMenu", categories);
        }

        public ActionResult BooksTips(string term)
        {
            var books = db.Books.Where(x => !x.Hidden && x.Title.ToLower().Contains(term.ToLower()))
                .Take(5).Select(x => new { label = x.Title });

            return Json(books, JsonRequestBehavior.AllowGet);
        }

    }
}