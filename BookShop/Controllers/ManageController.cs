﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using BookShop.App_Start;
using BookShop.DAL;
using BookShop.Infrastructure;
using BookShop.Models;
using BookShop.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using WebGrease;


namespace BookShop.Controllers
{
    [Authorize]
    public class ManageController : Controller
    {
        private BookShopContext db = new BookShopContext();

        //private static Logger logger = LogManager.GetCurrentClassLogger();
        //private BookShopContext db;
        //private IMailService mailService;

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            Error
        }

        //public ManageController(BookShopContext context, IMailService mailService)
        //{
        //    this.db = context;
        //    this.mailService = mailService;
        //}

        //public ManageController(ApplicationUserManager userManager)
        //{
        //    UserManager = userManager;
        //}

        private ApplicationUserManager _userManager;

        public ApplicationUserManager UserManager
        {
            get { return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>(); }
            private set { _userManager = value; }
        }

        private IAuthenticationManager AuthenticationManager
        {
            get { return HttpContext.GetOwinContext().Authentication; }
        }

        // GET: Manage
        public async Task<ActionResult> Index(ManageMessageId? message)
        {
            //var name = User.Identity.Name;
            //logger.Info("Admin główna | " + name);

            if (TempData["ViewData"] != null)
            {
                ViewData = (ViewDataDictionary) TempData["ViewData"];
            }

            if (User.IsInRole("Admin"))
                ViewBag.UserIsAdmin = true;
            else
                ViewBag.UserIsAdmin = false;

            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user == null)
            {
                return View("Error");
            }

            var model = new ManageCredentialsViewModel
            {
                Message = message,
                DaneUzytkownika = user.UserData
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangeProfile([Bind(Prefix = "DaneUzytkownika")] UserData daneUzytkownika)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                user.UserData = daneUzytkownika;
                var result = await UserManager.UpdateAsync(user);

                AddErrors(result);
            }

            if (!ModelState.IsValid)
            {
                TempData["ViewData"] = ViewData;
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(
            [Bind(Prefix = "ChangePasswordViewModel")] ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                TempData["ViewData"] = ViewData;
                return RedirectToAction("Index");
            }

            var result =
                await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInAsync(user, isPersistent: false);
                }
                return RedirectToAction("Index", new {Message = ManageMessageId.ChangePasswordSuccess});
            }
            AddErrors(result);

            if (!ModelState.IsValid)
            {
                TempData["ViewData"] = ViewData;
                return RedirectToAction("Index");
            }

            var message = ManageMessageId.ChangePasswordSuccess;
            return RedirectToAction("Index", new {Message = message});
        }


        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("password-error", error);
            }
        }

        private async Task SignInAsync(ApplicationUser user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie,
                DefaultAuthenticationTypes.TwoFactorCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties {IsPersistent = isPersistent},
                await user.GenerateUserIdentityAsync(UserManager));
        }


        public ActionResult ListOfOrders()
        {
            //var name = User.Identity.Name;
            //logger.Info("Admin zamowienia | " + name);

            bool isAdmin = User.IsInRole("Admin");
            ViewBag.UserIsAdmin = isAdmin;

            IEnumerable<Order> zamowieniaUzytkownika;

            // Dla administratora zwracamy wszystkie zamowienia
            if (isAdmin)
            {
                zamowieniaUzytkownika = db.Orders.Include("OrderPositions").OrderByDescending(o => o.AddDate).ToArray();
            }
            else
            {
                var userId = User.Identity.GetUserId();
                zamowieniaUzytkownika = db.Orders.Where(o => o.UserId == userId).Include("OrderPositions").OrderByDescending(o => o.AddDate).ToArray();
            }

            return View(zamowieniaUzytkownika);
        }


        [HttpPost]
        [Authorize(Roles = "Admin")]
        public OrderState ChangeStateOrder(Order zamowienie)
        {
            Order zamowienieDoModyfikacji = db.Orders.Find(zamowienie.OrderId);
            zamowienieDoModyfikacji.OrderState = zamowienie.OrderState;
            db.SaveChanges();

            //if (zamowienieDoModyfikacji.OrderState == OrderState.Completed)
            //{
            //    this.mailService.WyslanieZamowienieZrealizowaneEmail(zamowienieDoModyfikacji);
            //}

            return zamowienie.OrderState;
        }

        [Authorize(Roles = "Admin")]
        public ActionResult AddBook(int? bookId, bool? confirm)
        {
            Book book;
            if (bookId.HasValue)
            {
                ViewBag.EditMode = true;
                book = db.Books.Find(bookId);
            }
            else
            {
                ViewBag.EditMode = false;
                book = new Book();
            }

            var result = new EditBookViewModel();
            result.Categories = db.Categories.ToList();
            result.Book = book;
            result.Confirm = confirm;

            return View(result);
        }


        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult AddBook(EditBookViewModel model, HttpPostedFileBase file)
        {
            if (model.Book.BookId > 0)
            {
                // modyfikacja ksiazki
                db.Entry(model.Book).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("AddBook", new { confirm = true });
            }
            else
            {
                // Sprawdzenie, czy użytkownik wybrał plik
                if (file != null && file.ContentLength > 0)
                {
                    if (ModelState.IsValid)
                    {
                        // Generowanie pliku
                        var fileExt = Path.GetExtension(file.FileName);
                        var filename = Guid.NewGuid() + fileExt;

                        var path = Path.Combine(Server.MapPath(AppConfig.IconsBooksFolderRelative), filename);
                        file.SaveAs(path);

                        model.Book.NamePic = filename;
                        model.Book.AddDate = DateTime.Now;

                        db.Entry(model.Book).State = EntityState.Added;
                        db.SaveChanges();

                        return RedirectToAction("AddBook", new { confirm = true });
                    }
                    else
                    {
                        var categories = db.Categories.ToList();
                        model.Categories = categories;
                        return View(model);
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Nie wskazano pliku");
                    var categories = db.Categories.ToList();
                    model.Categories = categories;
                    return View(model);
                }



            }
            
        }


        [Authorize(Roles = "Admin")]
        public ActionResult HideBook(int bookId)
        {
            var book = db.Books.Find(bookId);
            book.Hidden = true;
            db.SaveChanges();

            return RedirectToAction("AddBook", new { confirm = true });
        }

        [Authorize(Roles = "Admin")]
        public ActionResult ShowBook(int bookId)
        {
            var book = db.Books.Find(bookId);
            book.Hidden = false;
            db.SaveChanges();

            return RedirectToAction("AddBook", new { confirm = true });
        }
    }
}


