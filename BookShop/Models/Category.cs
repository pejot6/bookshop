﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BookShop.Models
{
    public class Category
    {
        public int CategoryId { get; set; }
        [Required(ErrorMessage = "Nazwa wymagana")]
        [StringLength(50)]
        public string Name { get; set; }
        [Required(ErrorMessage = "Opis wymagany")]
        public string Description { get; set; }
        public string NamePic { get; set; }

        public virtual ICollection<Book> Books { get; set; }
    }
}