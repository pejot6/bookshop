﻿namespace BookShop.Models
{
    public class OrderPosition
    {
        public int OrderPositionId { get; set; }
        public int OrderId { get; set; }
        public int BookId { get; set; }
        public int Count { get; set; }
        public decimal PurchasePrice { get; set; }

        public virtual Book Book { get; set; }
        public virtual Order Order { get; set; }
    }
}