﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BookShop.Models
{
    public class Book
    {
        public int BookId { get; set; }
        public int CategoryId { get; set; }
        [Required(ErrorMessage = "Tytuł wymagany")]
        [StringLength(50)]
        public string Title { get; set; }
        [Required(ErrorMessage = "Autor wymagany")]
        [StringLength(50)]
        public string Writer { get; set; }
        public DateTime AddDate { get; set; }
        [StringLength(50)]
        public string NamePic { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public bool Bestseller { get; set; }
        public bool Hidden { get; set; }
     
        public virtual Category Category { get; set; }
    }
}