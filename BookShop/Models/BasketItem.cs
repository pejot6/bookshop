﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookShop.Models
{
    public class BasketItem
    {
        public Book Book { get; set; }
        public int Number { get; set; }
        public decimal Value { get; set; }
    }
}