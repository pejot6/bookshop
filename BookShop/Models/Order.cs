﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BookShop.Models
{
    public class Order
    {
        public int OrderId { get; set; }

        public string UserId { get; set; }

        public virtual ApplicationUser User { get; set; }

        [Required(ErrorMessage = "Imię wymagane")]
        [StringLength(50)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Nazwisko wymagane")]
        [StringLength(50)]
        public string Surname { get; set; }

        [Required(ErrorMessage = "Ulica wymagana")]
        [StringLength(50)]
        public string Street { get; set; }

        [Required(ErrorMessage = "Miasto wymagane")]
        [StringLength(50)]
        public string City { get; set; }

        [Required(ErrorMessage = "Kod pocztowy wymagany")]
        [StringLength(6)]
        public string ZipCode { get; set; }

        [Required(ErrorMessage = "Numer telefonu wymagany")]
        [StringLength(20)]
        [RegularExpression(@"(\+\d{2})*[\d\s-]+", ErrorMessage = "Błędny format numeru telefonu.")]
        public string Telephone { get; set; }

        [Required(ErrorMessage = "Adres e-mail wymagany")]
        [EmailAddress(ErrorMessage = "Błędny format adresu e-mail")]
        public string Email { get; set; }
        public string Comment { get; set; }
        public DateTime AddDate { get; set; }
        public OrderState OrderState { get; set; }
        public decimal TotalPrice { get; set; }

        public List<OrderPosition> OrderPositions { get; set; }
    }

    public enum OrderState
    {
        New, 
        Completed
    }
}