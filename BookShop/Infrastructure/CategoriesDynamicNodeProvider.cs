﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BookShop.DAL;
using BookShop.Models;
using MvcSiteMapProvider;

namespace BookShop.Infrastructure
{
    public class CategoriesDynamicNodeProvider : DynamicNodeProviderBase
    {
        private BookShopContext db = new BookShopContext();

        public override IEnumerable<DynamicNode> GetDynamicNodeCollection(ISiteMapNode nodee)
        {
            var returnValue = new List<DynamicNode>();

            foreach (Category category in db.Categories)
            {
                DynamicNode node = new DynamicNode();
                node.Title = category.Name;
                node.Key = "Kategoria" + category.CategoryId;
                node.RouteValues.Add("name", category.Name);
                returnValue.Add(node);
            }

            return returnValue;
        }
    }
}