﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookShop.Infrastructure
{
    public class Const
    {
        public const string NewCacheKey = "NewCacheKey";
        public const string BestsellerCacheKey = "BestsellerCacheKey";
        public const string CategoryCacheKey = "CategoryCacheKey";
        public const string BasketSessionKey = "BasketSessionKey";
    }
}