﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BookShop.DAL;
using BookShop.Models;
using MvcSiteMapProvider;

namespace BookShop.Infrastructure
{
    public class BooksDetailsDynamicNodeProvider : DynamicNodeProviderBase
    {
        private BookShopContext db = new BookShopContext();

        public override IEnumerable<DynamicNode> GetDynamicNodeCollection(ISiteMapNode nodee)
        {
            var returnValue = new List<DynamicNode>();

            foreach (Book book in db.Books)
            {
                DynamicNode node = new DynamicNode();
                node.Title = book.Title;
                node.Key = "Ksiazka_" + book.BookId;
                node.ParentKey = "Kategoria_" + book.CategoryId;
                node.RouteValues.Add("id", book.BookId);
                returnValue.Add(node);
            }

            return returnValue;
        }
    }
}