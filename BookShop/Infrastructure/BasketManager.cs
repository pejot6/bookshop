﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BookShop.DAL;
using BookShop.Models;

namespace BookShop.Infrastructure
{
    public class BasketManager
    {
        private BookShopContext db;
        private ISessionManager session;

        public BasketManager(ISessionManager session, BookShopContext db)
        {
            this.session = session;
            this.db = db;
        }

        public List<BasketItem> DownloadBasket()
        {
            List<BasketItem> basket;

            if (session.Get<List<BasketItem>>(Const.BasketSessionKey)==null)
            {
                basket = new List<BasketItem>();
            }
            else
            {
                basket = session.Get<List<BasketItem>>(Const.BasketSessionKey) as List<BasketItem>;
            }

            return basket;
        }

        public void AddToBasket(int bookId)
        {
            var basket = DownloadBasket();
            var basketItem = basket.Find(x => x.Book.BookId == bookId);

            if (basketItem != null)
                basketItem.Number++;
            else
            {
                var bookToAdd = db.Books.Where(x => x.BookId == bookId).SingleOrDefault();

                if (bookToAdd != null)
                {
                    var newBasketItem = new BasketItem()
                    {
                        Book = bookToAdd,
                        Number = 1,
                        Value = bookToAdd.Price
                    };
                    basket.Add(newBasketItem);
                }
            }
            session.Set(Const.BasketSessionKey, basket);
        }

        public int DeleteFromBasket(int kursId)
        {
            var koszyk = DownloadBasket();
            var basketItem = koszyk.Find(k => k.Book.BookId == kursId);

            if (basketItem != null)
            {
                if (basketItem.Number > 1)
                {
                    basketItem.Number--;
                    return basketItem.Number;
                }
                else
                {
                    koszyk.Remove(basketItem);
                }
            }

            return 0;
        }

        public decimal DownloadBasketValue()
        {
            var basket = DownloadBasket();
            return basket.Sum(x => (x.Number * x.Book.Price));
        }

        public int DownloadNumberBasketItem()
        {
            var basket = DownloadBasket();
            int number = basket.Sum(x => x.Number);
            return number;
        }

        public Order AddOrder(Order newOrder, string userId)
        {
            var basket = DownloadBasket();
            newOrder.AddDate = DateTime.Now;
            newOrder.UserId = userId;

            db.Orders.Add(newOrder);
            if(newOrder.OrderPositions == null)
                newOrder.OrderPositions = new List<OrderPosition>();

            decimal basketValue = 0;

            foreach (var basketItem in basket)
            {
                var newOrderPosition = new OrderPosition()
                {
                    BookId = basketItem.Book.BookId,
                    Count = basketItem.Number,
                    PurchasePrice = basketItem.Value
                };
                basketValue += (basketItem.Number*basketItem.Book.Price);
                newOrder.OrderPositions.Add(newOrderPosition);
            }
            newOrder.TotalPrice = basketValue;
            db.SaveChanges();

            return newOrder;
        }

        public void EmptyBasket()
        {
            session.Set<List<BasketItem>>(Const.BasketSessionKey, null);
        }
    }
}