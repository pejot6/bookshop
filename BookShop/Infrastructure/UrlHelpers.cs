﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookShop.Infrastructure
{
    public static class UrlHelpers
    {
        public static string IconsCategoriesPatch(this UrlHelper helper, string nameIconCategory)
        {
            var iconsCategoryFolder = AppConfig.iconsCategoryFolderRelative;
            var path = Path.Combine(iconsCategoryFolder, nameIconCategory);
            var pathabsolute = helper.Content(path);
            
            return pathabsolute;
        }

        public static string IconsBooksPatch(this UrlHelper helper, string nameIconBooks)
        {
            var iconsBooksFolder = AppConfig.IconsBooksFolderRelative;
            var path = Path.Combine(iconsBooksFolder, nameIconBooks);
            var pathabsolute = helper.Content(path);

            return pathabsolute;
        }
    }
}