﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using BookShop.Migrations;

namespace BookShop.Infrastructure
{
    public class AppConfig
    {
        private static string _iconsCategoryFolderRelative = ConfigurationManager.AppSettings["IconsCategoryFolder"];

        public static string iconsCategoryFolderRelative
        {
            get { return _iconsCategoryFolderRelative; }
        }

        private static string _IconsBooksFolderRelative = ConfigurationManager.AppSettings["IconsBooksFolder"];
        public static string IconsBooksFolderRelative => _IconsBooksFolderRelative;
    }
}